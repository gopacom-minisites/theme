<?php
/** @var array $attributes */

$args = [
    'post_type'      => 'badge',
    'post_status'    => 'publish',
    'posts_per_page' => -1,
    'order'          => 'ASC',
    'meta_key'       => 'weight',
    'orderby'        => 'meta_value'
];

// Maybe it's multilingual
if (function_exists('pll_current_language')) {
    $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
}


// Custom query.
$myquery = new WP_Query($args);
$badges  = $myquery->get_posts();

?>


</main>
<footer>
  <div class="container-xl px-md-4 px-xl-0">
    <div class="row footer-menu">
      <div class="col-12 col-md-12 col-lg-4 order-2 order-md-1 mt-4 mt-md-0">
        <figure class="col-8 footer-menu-logo">
          <a href="<?= bloginfo('url') ?>">
            <img class="d-inline-block align-top"
                 src="<?= get_option('p_logo_footer') ?: 'https://via.placeholder.com/320x240?text=logo' ?>"
                 alt='<?= bloginfo('title') ?>'>
          </a>
        </figure>
        <div class="col-12 d-flex copyright-contact-wrapper">
          <div class="col-6 copyright">
            &copy; <?= pll__('copyright') ?> <?= date('Y') ?>
          </div>
          <div class="col-6 contact-information">
            <p>
                <?php pll_e('street-and-number'); ?>
            </p>
            <p>
                <?php pll_e('postcode-and-location'); ?>
            </p>
            <p>
                <?php pll_e('country'); ?>
            </p>
          </div>
        </div>

        <div class="col-12 d-flex socials-phone-wrapper">
          <div class="col-6" id="social-icons">
              <?php if (get_option('p_instagram_link')) { ?>
                <div class="social-icon">
                  <a href="<?= get_option('p_instagram_link') ?>"
                     target="_blank">
                    <i class="fab fa-instagram"></i>
                  </a>
                </div>
              <?php } ?>
              <?php if (get_option('p_facebook_link')) { ?>
                <div class="social-icon">
                  <a href="<?= get_option('p_facebook_link') ?>"
                     target="_blank">
                    <i class="fab fa-facebook-f"></i>
                  </a>
                </div>
              <?php } ?>
              <?php if (get_option('p_linkedin_link')) { ?>
                <div class="social-icon">
                  <a href="<?= get_option('p_linkedin_link') ?>"
                     target="_blank">
                    <i class="fab fa-linkedin-in"></i>
                  </a>
                </div>
              <?php } ?>
          </div>
          <p class="col-6">
            <a href="tel:<?php pll_e('machine-phone-number'); ?>">
                <?php pll_e('human-phone-number'); ?>
            </a>
          </p>
        </div>
      </div>
      <div class="col-12 col-md-12 col-lg-4 offset-lg-1 d-flex order-1 order-md-2">
        <div id="footer-nav-pages" class="col-6">
          <h6><?php pll_e('links-to-pages') ?></h6>
            <?php
            wp_nav_menu([
                'theme_location'  => 'footer-menu',
                'menu_class'      => 'footer-menu-links',
                'container_id'    => 'footerMenuPages',
                'container_class' => 'col-12 footer-nav-links',
                'bootstrap'       => true,
            ]);
            ?>
        </div>
        <div id="footer-nav-policies" class="col-6">
          <h6><?php
              pll_e('links to policies'); ?></h6>
            <?php
            wp_nav_menu([
                'theme_location'  => 'footer-menu-policies',
                'menu_class'      => 'footer-menu-links',
                'container_id'    => 'footerMenuPolicies',
                'container_class' => 'col-12 footer-nav-links',
                'bootstrap'       => true,
            ]);
            ?>
        </div>
      </div>
        <?php if ($badges) { ?>
          <div class="col-12 col-md-12 col-lg-3 order-3">
              <?php if (count($badges)) { ?>
                <div class="col-12 certs-org-title">
                  <h6><?= pll__('certificates-and-organisations') ?></h6>
                </div>
              <?php } ?>
            <div class="col-12 d-flex certs-org-logo-wrapper">
                <?php foreach ($badges as $badge) { ?>
                  <figure class="col-5 footer-certs-org-logo">
                      <?php if (get_field('url', $badge)) { ?>
                        <a href="<?= get_field('url', $badge) ?>" target="_blank"></a>
                      <?php } ?>
                    <img class="d-inline-block align-top"
                         src="<?= get_field('image', $badge) ?: 'https://via.placeholder.com/320x240?text='.get_the_title($badge)  ?>"
                         alt='<?= get_the_title($badge) ?>'>
                  </figure>
                <?php } ?>
            </div>
          </div>
        <?php } ?>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
