<!doctype html>
<html lang="<?= str_replace("_", "-", bloginfo('language')); ?>" class="load-hidden">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        :root {
            --primary-font-color: <?= get_option('p_primary_font_color') ? get_option('p_primary_font_color') : 'black' ?>;
            --secondary-font-color: <?= get_option('p_secondary_font_color') ? get_option('p_secondary_font_color') : 'white' ?>;
            --primary-color: <?= get_option('p_primary_color') ? get_option('p_primary_color') : 'darkblue' ?>;
            --secondary-color: <?= get_option('p_secondary_color') ? get_option('p_secondary_color') : 'blue' ?>;
            --supporting-color: <?= get_option('p_supporting_color') ? get_option('p_supporting_color') : 'grey' ?>;
            --primary-link-color: <?= get_option('p_primary_link_color') ? get_option('p_primary_link_color') : 'lightblue' ?>;
            --primary-link-hover-color: <?= get_option('p_primary_link_hover_color') ? get_option('p_primary_link_hover_color') : 'blue' ?>;
            --secondary-link-color: <?= get_option('p_secondary_link_color') ? get_option('p_secondary_link_color') : 'red' ?>;
            --secondary-link-hover-color: <?= get_option('p_secondary_link_hover_color') ? get_option('p_secondary_link_hover_color') : 'orange' ?>;
            --border-radius: <?= get_option('p_border_radius') !== '' ? get_option('p_border_radius') : '0.5' ?>rem;
            --primary-font: 'Inter', Helvetica, Arial, sans-serif;
            --secondary-font: <?= get_option('p_secondary_font') ? get_option('p_secondary_font') : "'Nunito', Helvetica, Arial, sans-serif" ?>;
            --secondary-font-weight: <?= get_option('p_secondary_font_weight') ? get_option('p_secondary_font_weight') : '700' ?>;
        }
    </style>
    <?php wp_head(); ?>

</head>
<body>
<?php
$languages = pll_the_languages(['raw' => 1]);
ksort($languages);
?>
<header>
    <?php
        $menuVariant = get_option('p_menu_variant') ? get_option('p_menu_variant') : 'default';
        if ($menuVariant === 'default') {
            include get_template_directory() . '/menus/default.php';
        }
        if ($menuVariant === 'flyout_small') {
            include get_template_directory() . '/menus/flyout_small.php';
        }
        if ($menuVariant === 'flyout_large') {
            include get_template_directory() . '/menus/flyout_large.php';
        }
    ?>
</header>

<main>
    <?php
    if (get_field('header_image')) {
        ?>
        <div class="container-fluid px-0 header-image">
            <img src="<?= get_field('header_image')['url'] ?>"/>
        </div>
        <?php
    }
    ?>
