
function string_to_slug (str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
}

jQuery( document ).ready(function($) {

   /* $("img").each( function ()
    {
        const _this = $(this)

        $.ajax({
            url:$(this).attr('src'),
            type:'HEAD',
            async: false,
            error:
              function(e)
              {
                  if (e.status === 404) {
                      $(_this).addClass('d-none');
                  }
              }
        });
    });*/

    $('.menu-button').on('click', function(event) {
        event.stopPropagation();
        $(this).toggleClass('open');
        $('#nav-menu-overlay').toggleClass('open');
        $('#nav-menu-overlay-small').toggleClass('open');
    });

    /**
     * Menu hover for languages
     **/

    $('.other-language a').hover( function() {
        $('.language-dropdown > .dropdown-toggle').addClass('hoverstate');
    }, function() {
        $('.language-dropdown > .dropdown-toggle').removeClass('hoverstate');
    });

    $('.other-language a').click( function() {
        $('.language-dropdown > .dropdown-toggle').addClass('hoverstate');
    }, function() {
        $('.language-dropdown > .dropdown-toggle').removeClass('hoverstate');
    });

    $('.language-dropdown > .dropdown-toggle , .other-language').hover( function() {
        $('.language-dropdown > .dropdown-toggle').siblings('.other-language').addClass('show');
    }, function () {
        $('.language-dropdown > .dropdown-toggle').siblings('.other-language').removeClass('show');
    });

    /**
     * Adding css after scroll for menu-top
     **/
    let scrollpage = $('body , html');
    $(document).scroll(function (e) {
        if ($(document).scrollTop() > 40) {
            $('.menu-top').addClass('navbar-scroll')
        } else {
            $('.menu-top').removeClass('navbar-scroll')
        }
    });

    $('.carousel-inner-wrapper').slick({
        dots: true,
        infinite: true,
        //speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 8000,
        adaptiveHeight: true,
        arrows: true,
        prevArrow: '<i class="fas fa-arrow-left prev"></i>',
        nextArrow: '<i class="fas fa-arrow-right next"></i>',

    });

});

// Check if default menu is true so main pushed down
let defaultMenu = document.querySelector('div.menu-top');
if (defaultMenu) {
    let height = defaultMenu.offsetHeight;
    let main = document.querySelector('main');
    main.style.paddingTop = height + 'px';
}
