

(function(){
  tinymce.create('tinymce.plugins.MyPluginName', {
    init: function(ed, url){

      // CTA BUTTON
      ed.addButton('myctabtn', {
        title: 'Call to action',
        cmd: 'myCtaBtnCmd',
        image: url + '/arrow.png'
      });
      ed.addCommand('myCtaBtnCmd', function(){
        var selectedText = ed.selection.getContent({format: 'html'});
        var win = ed.windowManager.open({
          title: 'CTA Properties',
          body: [
            {
              type: 'textbox',
              name: 'label',
              label: 'Label',
              minWidth: 500,
              value: ''
            },
            {
              type: 'textbox',
              name: 'url',
              label: 'Url',
              minWidth: 500,
              value : ''
            },
            {
              type: 'listbox',
              name: 'variant',
              label: 'Variant',
              minWidth: 500,
              values: [
                { text: 'Primary', value: 'primary' },
                { text: 'Secondary', value: 'secondary' },
                { text: 'Grey', value: 'grey' },
                { text: 'Blue', value: 'blue' },
                { text: 'Default', value: 'default' }
              ],
            },
            {
              type: 'checkbox',
              name: 'newtab',
              label: 'Newtab?',
              minWidth: 500,
              value: ''
            }
          ],
          buttons: [
            {
              text: "Ok",
              subtype: "primary",
              onclick: function() {
                win.submit();
              }
            },
            {
              text: "Cancel",
              onclick: function() {
                win.close();
              }
            }
          ],
          onsubmit: function(e){
            var params = [];
            if( e.data.label.length > 0 ) {
              params.push('label="' + e.data.label + '"');
            }
            if( e.data.url.length > 0 ) {
              params.push('url="' + e.data.url + '"');
            }
            if( e.data.variant.length > 0 ) {
              params.push('variant="' + e.data.variant + '"');
            }
            if( e.data.newtab ) {
              params.push('newtab="1"');
            }
            if( params.length > 0 ) {
              paramsString = ' ' + params.join(' ');
            }
            var returnText = '[cta ' + paramsString + ']';
            ed.execCommand('mceInsertContent', 0, returnText);
          }
        });
      });


      // WHOS WHO
      ed.addButton('mywhoswhobtn', {
        title: 'Whos Who',
        cmd: 'myWhosWhoBtnCmd',
        image: url + '/buddy.svg'
      });
      ed.addCommand('myWhosWhoBtnCmd', function(){
        var selectedText = ed.selection.getContent({format: 'html'});
        var win = ed.windowManager.open({
          title: 'Whoswho Properties',
          body: [
            {
              type: 'textbox',
              name: 'name',
              label: 'Name',
              minWidth: 500,
              value: ''
            },
            {
              type: 'textbox',
              name: 'icon',
              label: 'Icon',
              minWidth: 500,
              value : ''
            },
            {
              type: 'textbox',
              name: 'position',
              label: 'Position',
              minWidth: 500,
              value : ''
            },
          ],
          buttons: [
            {
              text: "Ok",
              subtype: "primary",
              onclick: function() {
                win.submit();
              }
            },
            {
              text: "Cancel",
              onclick: function() {
                win.close();
              }
            }
          ],
          onsubmit: function(e){
            var params = [];
            if( e.data.name.length > 0 ) {
              params.push('name="' + e.data.name + '"');
            }
            if( e.data.icon.length > 0 ) {
              params.push('icon="' + e.data.icon + '"');
            }
            if( e.data.position.length > 0 ) {
              params.push('position="' + e.data.position + '"');
            }
            if( params.length > 0 ) {
              paramsString = ' ' + params.join(' ');
            }
            var returnText = '[whoswho ' + paramsString + ']';
            ed.execCommand('mceInsertContent', 0, returnText);
          }
        });
      });


      // IMPRESSIVES
      ed.addButton('myimpressivebtn', {
        title: 'Impressive',
        cmd: 'myImpressiveBtnCmd',
        image: url + '/performance.svg'
      });
      ed.addCommand('myImpressiveBtnCmd', function(){
        var selectedText = ed.selection.getContent({format: 'html'});
        var win = ed.windowManager.open({
          title: 'Impressive Properties',
          body: [
            {
              type: 'textbox',
              name: 'number',
              label: 'Number',
              minWidth: 500,
              value: ''
            },
            {
              type: 'textbox',
              name: 'label',
              label: 'Label',
              minWidth: 500,
              value : ''
            },
          ],
          buttons: [
            {
              text: "Ok",
              subtype: "primary",
              onclick: function() {
                win.submit();
              }
            },
            {
              text: "Cancel",
              onclick: function() {
                win.close();
              }
            }
          ],
          onsubmit: function(e){
            var params = [];
            if( e.data.number.length > 0 ) {
              params.push('number="' + e.data.number + '"');
            }
            if( e.data.label.length > 0 ) {
              params.push('label="' + e.data.label + '"');
            }
            if( params.length > 0 ) {
              paramsString = ' ' + params.join(' ');
            }
            var returnText = '[impressive ' + paramsString + ']';
            ed.execCommand('mceInsertContent', 0, returnText);
          }
        });
      });


      // GO IMG
      ed.addButton('mygoimgbtn', {
        title: 'Go Image',
        cmd: 'myGoimgBtnCmd',
        image: url + '/goimg.svg'
      });
      ed.addCommand('myGoimgBtnCmd', function(){
        var selectedText = ed.selection.getContent({format: 'html'});
        var win = ed.windowManager.open({
          title: 'Go Image Properties',
          body: [
            {
              type: 'textbox',
              name: 'image',
              label: 'Image',
              minWidth: 500,
              value: ''
            },
            {
              type: 'textbox',
              name: 'url',
              label: 'Url',
              minWidth: 500,
              value: ''
            },
            {
              type: 'textbox',
              name: 'label',
              label: 'Label',
              minWidth: 500,
              value : ''
            },
          ],
          buttons: [
            {
              text: "Ok",
              subtype: "primary",
              onclick: function() {
                win.submit();
              }
            },
            {
              text: "Cancel",
              onclick: function() {
                win.close();
              }
            }
          ],
          onsubmit: function(e){
            var params = [];
            if( e.data.image.length > 0 ) {
              params.push('image="' + e.data.image + '"');
            }
            if( e.data.url.length > 0 ) {
              params.push('url="' + e.data.url + '"');
            }
            if( e.data.label.length > 0 ) {
              params.push('label="' + e.data.label + '"');
            }
            if( params.length > 0 ) {
              paramsString = ' ' + params.join(' ');
            }
            var returnText = '[goimg ' + paramsString + ']';
            ed.execCommand('mceInsertContent', 0, returnText);
          }
        });
      });


      // POI
      ed.addButton('mypoibtn', {
        title: 'POI',
        cmd: 'myPoiBtnCmd',
        image: url + '/poi.svg'
      });
      ed.addCommand('myPoiBtnCmd', function(){
        var selectedText = ed.selection.getContent({format: 'html'});
        var win = ed.windowManager.open({
          title: 'POI Properties',
          body: [
            {
              type: 'textbox',
              name: 'icon',
              label: 'Icon',
              minWidth: 500,
              value: ''
            },
            {
              type: 'textbox',
              name: 'title',
              label: 'Title',
              minWidth: 500,
              value: ''
            },
          ],
          buttons: [
            {
              text: "Ok",
              subtype: "primary",
              onclick: function() {
                win.submit();
              }
            },
            {
              text: "Cancel",
              onclick: function() {
                win.close();
              }
            }
          ],
          onsubmit: function(e){
            var params = [];
            if( e.data.icon.length > 0 ) {
              params.push('icon="' + e.data.icon + '"');
            }
            if( e.data.title.length > 0 ) {
              params.push('title="' + e.data.title + '"');
            }
            if( params.length > 0 ) {
              paramsString = ' ' + params.join(' ');
            }
            var returnText = '[poi ' + paramsString + ']lorem ipsum dolor smet....[/poi]';
            ed.execCommand('mceInsertContent', 0, returnText);
          }
        });
      });

    },
    getInfo: function() {
      return {
        longname : 'My Custom Buttons',
        author : 'Content Coffee',
        authorurl : 'https://www.contentcoffee.com',
        version : "1.0"
      };
    }
  });
  tinymce.PluginManager.add( 'mytinymceplugin', tinymce.plugins.MyPluginName );
})();


