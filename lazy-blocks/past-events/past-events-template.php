<?php
/** @var array $attributes */


$args = [
    'post_type' => 'event',
    'post_status' => 'publish',
    'posts_per_page' => $attributes['amount_of_past_events'],
    'order' => 'DESC',
    'orderby' => 'publish_date',
    'meta_key' => 'starting_date',
    'meta_query' => [
        'key' => 'starting_date',
        'value' => date('Ymd'),
        'type' => 'DATE',
        'compare' => '<'
    ]
];

// Maybe it's multilingual
if (function_exists('pll_current_language')) {
    $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
}
?>

<div class="container-md">
    <div class="row lazyblock-past-events justify-content-center">
        <div class="col-12 col-lg-9 events-wrapper">
            <?php if ($attributes["title"] ?? '') { ?>
                <div class="row">
                    <div class="col <?= $attributes["textalignment"]; ?>">
                        <h2 class="title"><?= $attributes["title"] ?></h2>
                    </div>
                </div>
            <?php } ?>
            <?php
            // Custom query.
            $myquery = new WP_Query($args);
            $events = $myquery->get_posts();
            ?>
            <?php if ($events) { ?>

                <?php foreach ($events as $event) { ?>
                    <div class="event-item-wrapper">
                        <div class="event-item-date"><?= get_field('starting_date', $event); ?></div>
                        <div class="event-item-title"><h4>
                                <a href="<?= get_the_permalink($event); ?>"><?= get_the_title($event); ?></a>
                            </h4></div>
                        <div class="event-item-excerpt">
                            <?= project_truncate(get_the_excerpt($event), '56'); ?>
                        </div>
                        <div class="event-item-cta"><a href="<?= get_the_permalink($event) ?>"><?= pll__('Learn more'); ?></a></div>
                    </div>
                <?php } ?>

            <?php } else { ?>
                <div class="row no-past-events">
                    <div class="col">
                        <p><?php pll_e('No Past Events'); ?></p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php
// Restore original post data.
wp_reset_postdata();
?>

