<span>
{{#include 'image-customs-style'}}
{{#if fullwidth}}
    {{{svg}}}
{{else}}
<div class="row lazyblock-svg">
    <div class="col-lg-12 text-center">
        {{{svg}}}
    </div>
</div>
{{/if}}
</span>
