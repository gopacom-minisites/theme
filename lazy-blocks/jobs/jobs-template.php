<?php
/** @var array $attributes */


$args = [
    'post_type' => 'job',
    'post_status' => 'publish',
    'posts_per_page' => $attributes['amount_of_jobs'],
    'order' => 'DESC',
    'orderby' => 'publish_date',
];

// Maybe it's multilingual
if (function_exists('pll_current_language')) {
    $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
} ?>

<div class="container-md">
    <div class="row lazyblock-jobs justify-content-center">
        <div class="col-12 col-lg-9 jobs-wrapper">
            <?php if ($attributes["title"] ?? '') { ?>
                <div class="row">
                    <div class="col <?= $attributes["textalignment"]; ?>">
                        <h2 class="title"><?= $attributes["title"] ?></h2>
                    </div>
                </div>
            <?php } ?>
<?php
// Custom query.
$myquery = new WP_Query($args);
$jobs = $myquery->get_posts();
?>
            <?php if ($jobs) { ?>

                <?php foreach ($jobs as $job) { ?>
                    <div class="row pt-5 job-item-wrapper">
                        <?php if (get_field('label', $job)) { ?>
                <div class="job-item-category"><?= get_field('label', $job); ?></div>
                <?php } ?>
                <div class="job-item-title"><h4>
                            <a href="<?= get_the_permalink($job); ?>"><?= get_the_title($job); ?></a>
                        </h4></div>
                    <div class="job-item-excerpt">
                        <?= project_truncate(get_the_excerpt($job), '56'); ?>
                    </div>
                    <div class="job-item-cta"><a href="<?= get_the_permalink($job) ?>"><?= pll__('That sounds like me'); ?></a></div>
                    </div>
                <?php } ?>

<?php } else { ?>
<div class="row no-jobs">
            <div class="col">
                <p><?php pll_e('No Jobs'); ?></p>
            </div>
</div>
<?php } ?>
        </div>
    </div>
</div>
        <?php
        // Restore original post data.
        wp_reset_postdata();
        ?>

