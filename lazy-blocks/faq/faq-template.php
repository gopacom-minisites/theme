<div class="container-md">
    <div class="row lazyblock-faq justify-content-center faqs card-group" id="{{#blockid}}">
        {{#each faq}}
        <div class="col-12 col-md-10 px-md-0 pb-3 faq">
            <div class="faq-wrapper card h-100" data-chapterid="">
                <a href="#faq-{{#blockid}}-{{@index}}" class="faq-titlelink collapsed"
                   aria-controls="faq-{{#blockid}}-{{@index}}" aria-expanded="false"
                   data-bs-toggle="collapse">
                    <div class="faq-title">
                        <h5>{{title}}</h5>
                        <div class="toggle-button">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </a>
                <div class="collapse faq-content" id="faq-{{#blockid}}-{{@index}}">
                    {{{content}}}
                </div>
            </div>
        </div>
        {{/each}}
    </div>
</div>
