<?php


$blockslug = 'faq';
$blocktitle = 'FAQ';

// START THE BLOCK
$block = [];
$block['slug'] = 'lazyblock/' . $blockslug;
$block['title'] = $blocktitle;
$block['icon'] = 'dashicons dashicons-info-outline';
$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';

// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = false;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [];
$ghostkit['supports']['spacings'] = false;
$ghostkit['supports']['display'] = false;
$ghostkit['supports']['scrollReveal'] = false;
$ghostkit['supports']['frame'] = false;
$ghostkit['supports']['customCSS'] = false;

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START THE CONTROLS
$controls = [];


// START REPEATER CONTROL
$control = [];
$control['label'] = 'FAQS';
$control['type'] = 'repeater';
$control['name'] = 'faq';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['rows_min'] = '1';
$control['rows_max'] = '';
$control['rows_label'] = 'FAQ {{title}}';
$control['rows_add_button_label'] = '+ Add FAQ';
//$control['rows_collapsible'] = true;
//$control['rows_collapsed'] = true;

$control_id = 'control-'.$blockslug.'-'.$control['name'];
$controls[$control_id] = $control;
// END REPEATER CONTROL


// START TEXT CONTROL
$control = [];
$control['label'] = 'Title';
$control['type'] = 'text';
$control['name'] = 'title';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = 'control-'.$blockslug.'-faq';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '500';

$control_id = 'control-' . $blockslug . '-faq-' . $control['name'];
$controls[$control_id] = $control;
// END TEXT CONTROL


// START CLASSICEDITOR CONTROL
$control = [];
$control['label'] = 'Content';
$control['type'] = 'classic_editor';
$control['name'] = 'content';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = 'control-'.$blockslug.'-faq';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-faq-' . $control['name'];
$controls[$control_id] = $control;
// END CLASSICEDITOR CONTROL


// END THE CONTROLS
$block['controls'] = $controls;


// START CODE
$code = [];
$code['output_method'] = 'html';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['frontend_html'] = file_get_contents(dirname(__FILE__) . '/' . $blockslug . '-template.php');
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['show_preview'] = 'never';
$code['single_output'] = true;

/// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
