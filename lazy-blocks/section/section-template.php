{{#ifEquals container 'fluid'}}
<div class="container-fluid lazyblock-section">
{{#if backgroundimage}}
    <div class="fluid-image" style="background: center / cover no-repeat url('{{backgroundimage.url}}');"></div>
{{/if}}
{{#if background-color}}
    <div class="fluid-color {{background-color}}"></div>
{{/if}}
        <div class="container-md">
        <div class="row no-gutters">
            <div class="col-lg-12">{{{content}}}</div>
        </div>
    </div>
</div>
{{/ifEquals}}
{{#ifEquals container 'md'}}
<div class="container-md lazyblock-section">
    {{#if backgroundimage}}
    <div class="container-image" style="background: center / cover no-repeat url('{{backgroundimage.url}}');"></div>
    {{/if}}
    {{#if background-color}}
    <div class="container-color {{background-color}}"></div>
    {{/if}}
    <div class="row no-gutters">
        <div class="col-lg-12">{{{content}}}</div>
    </div>
</div>
{{/ifEquals}}
