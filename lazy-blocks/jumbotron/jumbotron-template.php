<div class="row lazyblock-jumbotron{{#if control-textmode}} light-mode{{/if}}">
    {{#if backgroundimage}}
    <div class="full-image" style="background: center / cover no-repeat url('{{backgroundimage.url}}');"></div>
    {{else}}
    <div class="full-color {{background-color}}"></div>
    {{/if}}
    <div class="container-md">
        <div class="row mx-0 justify-content-center">
        <section class="col-12 col-lg-10 text-center">
            <h1>{{title}}</h1>
            {{#if subtitle}}
            <h2 class="subtitle mx-auto">{{subtitle}}</h2>
            {{/if}}
            {{#if content}}
            <div class="content-wrapper col-lg-10 mx-auto">{{{content}}}</div>
            {{/if}}
            {{#if ctalabel}}
            <a class="btn {{buttonstyle}} {{#if outline}}outline{{/if}}" href="{{ctaurl}}" target="{{target}}">{{ctalabel}}</a>
            {{/if}}
        </section>
        </div>
    </div>

</div>
