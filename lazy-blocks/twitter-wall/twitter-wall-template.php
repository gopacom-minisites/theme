<div class="row px-0 mx-0 lazyblock-twitter-wall">
    <div class="col-12 px-0 mx-auto" style="max-width: 100%;">
        <script src="https://walls.io/js/wallsio-widget-1.2.js" data-wallurl="{{wallurl}}" data-width="100%" data-height="500" data-lazyload="1"></script>
    </div>
</div>
