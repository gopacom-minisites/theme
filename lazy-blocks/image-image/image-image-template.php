<div class="container-md">
    <div class="row lazyblock-image-image {{flex-centered}}">
        <div class="col-lg-{{division}} col-12 mb-lg-0 mb-4 image-first">
            <img src="{{image.url}}" alt="{{image.alt}}" title="{{image.alt}}"/>
        </div>
        <div class="col-lg-{{#leftover division}} col-12 image-second">
            <img src="{{image2.url}}" alt="{{image2.alt}}" title="{{image2.alt}}"/>
        </div>
    </div>
</div>
