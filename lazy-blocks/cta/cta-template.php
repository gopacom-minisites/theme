<div class="row lazyblock-cta">
    <div class="col-lg-12 text-{{alignment}}">
        <a class="btn {{buttonstyle}} {{#if outline}}outline{{/if}}" href="{{url}}" target="{{target}}">{{label}}</a>
    </div>
</div>
