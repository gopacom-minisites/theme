<div class="container-md">
    <div class="row lazyblock-heading-3 {{alignment}}">
        <div class="col-12 col-lg-{{division}} text-{{textalignment}}">
            <h3>{{{text}}}</h3>
        </div>
    </div>
</div>
