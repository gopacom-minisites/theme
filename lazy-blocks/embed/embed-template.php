<div class="container-md">
    <div class="row mx-0 lazyblock-embed text-center">
        <div class="ratio ratio-16x9">
            {{{code}}}
        </div>
    </div>
</div>
