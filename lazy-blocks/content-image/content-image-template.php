<div class="container-md">
    <div class="row lazyblock-content-image {{flex-centered}}">
        <div class="col-lg-{{division}} col-12 px-lg-5 mb-lg-0 mb-4 {{#if inverse}} order-2 {{else}} order-1 {{/if}}">
            {{{content}}}
        </div>
        <div class="col-lg-{{#leftover division}} {{#if inverse}} order-1 {{else}} order-2 {{/if}}">
            <img class="on-the-fly-{{#blockid}}" src="{{image.url}}" alt="{{image.alt}}" title="{{image.alt}}"/>
        </div>
    </div>
</div>
