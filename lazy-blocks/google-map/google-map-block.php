<?php



$blockslug = 'google-map';
$blocktitle = 'Google map';

// START THE BLOCK
$block = [];
$block['slug'] = 'lazyblock/' . $blockslug;
$block['title'] = $blocktitle;
$block['icon'] = 'dashicons dashicons-media-text';
$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';

// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = false;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [];
$ghostkit['supports']['spacings'] = false;
$ghostkit['supports']['display'] = false;
$ghostkit['supports']['scrollReveal'] = false;
$ghostkit['supports']['frame'] = false;
$ghostkit['supports']['customCSS'] = false;

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START THE CONTROLS
$controls = [];


// START TEXT CONTROL
$control = [];
$control['label'] = 'Please fill in your address';
$control['type'] = 'text';
$control['name'] = 'query';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-'.$blockslug.'-'.$control['name'];
$controls[$control_id] = $control;
// END TEXT CONTROL

// START TEXT CONTROL
$control = [];
$control['label'] = 'Title';
$control['type'] = 'text';
$control['name'] = 'title';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '500';

$control_id = 'control-'.$blockslug.'-'.$control['name'];
$controls[$control_id] = $control;
// END TEXT CONTROL


// START TEXT CONTROL
$control = [];
$control['label'] = 'Street';
$control['type'] = 'text';
$control['name'] = 'street';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '50';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '500';

$control_id = 'control-'.$blockslug.'-'.$control['name'];
$controls[$control_id] = $control;
// END TEXT CONTROL


// START TEXT CONTROL
$control = [];
$control['label'] = 'Zip code + City';
$control['type'] = 'text';
$control['name'] = 'city';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '50';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '500';

$control_id = 'control-'.$blockslug.'-'.$control['name'];
$controls[$control_id] = $control;
// END TEXT CONTROL

// START TEXT CONTROL
$control = [];
$control['label'] = 'Country';
$control['type'] = 'text';
$control['name'] = 'country';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '50';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '500';

$control_id = 'control-'.$blockslug.'-'.$control['name'];
$controls[$control_id] = $control;
// END TEXT CONTROL

// START TEXT CONTROL
$control = [];
$control['label'] = 'Phone';
$control['type'] = 'text';
$control['name'] = 'phone';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '500';

$control_id = 'control-'.$blockslug.'-'.$control['name'];
$controls[$control_id] = $control;
// END TEXT CONTROL

// START TEXT CONTROL
$control = [];
$control['label'] = 'CTA Label';
$control['type'] = 'text';
$control['name'] = 'label';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '50';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-'.$blockslug.'-'.$control['name'];
$controls[$control_id] = $control;
// END TEXT CONTROL

// START URL
$control = [];
$control['label'] = 'CTA Url';
$control['type'] = 'url';
$control['name'] = 'url';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '50';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-'.$blockslug.'-'.$control['name'];
$controls[$control_id] = $control;
// END URL

// START BTN STYLE CONTROL
$control = [];
$control['label'] = 'Button Style';
$control['type'] = 'select';
$control['name'] = 'buttonstyle';
$control['default'] = 'btn-primary';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Primary',
        'value' => 'btn-primary'
    ],
    [
        'label' => 'Secondary',
        'value' => 'btn-secondary'
    ],
    [
        'label' => 'Supporting',
        'value' => 'btn-supporting'
    ]
];

$control['choices'] = $choices;
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END BTN STYLE CONTROL

// START TOGGLE
$control = [];
$control['label'] = 'Button Outline style';
$control['type'] = 'toggle';
$control['name'] = 'outline';
$control['default'] = false;
$control['checked'] = false;
$control['alongside_text'] = 'Outline';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END TOGGLE

// START RADIO
$control = [];
$control['label'] = 'Open Link in new tab?';
$control['type'] = 'radio';
$control['name'] = 'target';
$control['default'] = '_self';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Yes',
        'value' => '_blank'
    ],
    [
        'label' => 'No',
        'value' => '_self'
    ],
];

$control['choices'] = $choices;
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END RADIO

// START SELECT
$control = [];
$control['label'] = 'CTA Alignment';
$control['type'] = 'select';
$control['name'] = 'alignment';
$control['default'] = 'center';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Left',
        'value' => 'start'
    ],
    [
        'label' => 'Center',
        'value' => 'center'
    ],
    [
        'label' => 'Right',
        'value' => 'end'
    ]
];

$control['choices'] = $choices;
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END BTN STYLE CONTROL


// END THE CONTROLS
$block['controls'] = $controls;


// START CODE
$code = [];
$code['output_method'] = 'html';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['frontend_html'] = file_get_contents(dirname(__FILE__) . '/' . $blockslug . '-template.php');
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['show_preview'] = 'never';
$code['single_output'] = true;

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
