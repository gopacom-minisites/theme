<div class="container-md">
    <div class="row lazyblock-content {{alignment}}">
        <div class="col-12 col-lg-{{division}} text-{{textalignment}}">
            {{{content}}}
        </div>
    </div>
</div>
