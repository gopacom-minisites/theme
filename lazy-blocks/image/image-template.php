<div class="container-md">
    <div class="row lazyblock-image {{alignment}}">
        <div class="col-lg-8 d-flex">
            <img src="{{image.url}}" alt="{{image.alt}}" title="{{image.alt}}"/>
        </div>
    </div>
</div>
