<?php

$blockslug = 'cards';

// Start the block
$block             = [];
$block['title']    = 'Cards';
$block['icon']     = 'dashicons dashicons-images-alt';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// Holder for the controls
$controls = [];

// Content
$control                          = [];
$control['label']                 = 'Cards';
$control['name']                  = 'cards';
$control['type']                  = 'repeater';
$control['rows_min']              = '1';
$control['rows_max']              = '4';
$control['rows_label']            = 'Card {{#}}: {{title}}';
$control['rows_add_button_label'] = '+ Add Card';
$control['rows_collapsible']      = 'true';
$control['rows_collapsed']        = 'true';
$control['save_in_meta']          = false;
$control['child_of']              = '';
$control['hide_if_not_selected']  = 'false';

// Make an id.
$control_id = 'cards_cards';

// Add the control to the controls
$controls[$control_id] = $control;

// Image
$control                         = [];
$control['label']                = 'Image';
$control['name']                 = 'image';
$control['type']                 = 'image';
$control['save_in_meta']         = false;
$control['child_of']             = 'cards_cards';
$control['hide_if_not_selected'] = 'true';

// Make an id.
$control_id = 'cards-cards-image';

// Add the control to the controls
$controls[$control_id] = $control;

// Title
$control                         = [];
$control['label']                = 'Title';
$control['name']                 = 'title';
$control['type']                 = 'text';
$control['save_in_meta']         = false;
$control['child_of']             = 'cards_cards';
$control['hide_if_not_selected'] = 'false';
$control['default']              = '';

// Make an id.
$control_id            = 'cards_cards_title';
$controls[$control_id] = $control;


// Content
$control = [];
$control['label'] = 'Content';
$control['name'] = 'content';
$control['type'] = 'classic_editor';
$control['child_of'] = 'cards_cards';

// Make an id.
$control_id = 'cards_cards_content';
// Add the control to the controls
$controls[$control_id] = $control;


// Label
$control                         = [];
$control['label']                = 'CTA Label';
$control['name']                 = 'cta_label';
$control['type']                 = 'text';
$control['child_of']             = 'cards_cards';


// Make an id.
$control_id = 'cards_cards_cta_label';

// Add the control to the controls
$controls[$control_id] = $control;


// URL
$control                 = [];
$control['label']        = 'Url';
$control['name']         = 'url';
$control['type']         = 'url';
$control['save_in_meta'] = false;
$control['child_of']     = 'cards_cards';

// Make an id.
$control_id = 'cards_cards_url';
// Add the control to the controls
$controls[$control_id] = $control;


// Layout
$control = [];
$control['label'] = 'Layout';
$control['name'] = 'layout';
$control['type'] = 'radio';
$control['placement'] = 'inspector';
$control['required'] = true;
$control['default'] = '3';
$control['child_of'] = '';

$control['choices'] = [
    [
        'label' => '3 by 3',
        'value' => '3',
    ],
    [
        'label' => '4 by 4',
        'value' => '4',
    ],
];

// Make an id.
$control_id = 'cards_cards_layout';
// Add the control to the controls
$controls[$control_id] = $control;


// START TOGGLE
$control = [];
$control['label']                = 'Center all content?';
$control['name']                 = 'center';
$control['type']                 = 'toggle';
$control['placement']            = 'inspector';
$control['required']             = false;
$control['default']              = false;
$control['checked']              = true;
$control['child_of']             = '';
$control['alongside_text']       = 'Center';

// Make an id.
$control_id = 'cards_cards_center';
// Add the control to the controls
$controls[$control_id] = $control;


// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(dirname(__FILE__) . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
