<div class="container-md">
    <div class="row no-gutters row-cols-1 row-cols-lg-2 row-cols-xl-{{layout}} lazyblock-cards justify-content-lg-between">
        {{#ifEquals layout '3'}}
        {{#each cards}}
        <div class="col pb-4 d-flex">
            <div class="card h-100 px-0">
                <a href="{{url}}" class="card-link"></a>
                <div class="card-img-wrapper">
                    {{#if image}}
                    <img src="{{image.url}}" class="card-img-top"
                         alt="{{title}}">
                    <span class="image-line"></span>
                    {{/if}}
                </div>
                <div class="card-body flex-column h-100">
                    {{#if title}}
                    <div class="card-title">
                        <p>{{title}}</p>
                    </div>
                    {{/if}}
                    {{#if content}}
                    <div class="card-text">
                        {{{content}}}
                    </div>
                    {{/if}}
                </div>
                {{#if cta_label}}
                <div class="card-footer">
                    <a href="{{url}}" class="card-footer-link">{{cta_label}}</a>
                </div>
                {{/if}}
            </div>
        </div>
        {{/each}}
        {{/ifEquals}}

        {{#ifEquals layout '4'}}
        {{#each cards}}
        <div class="col pb-4">
            <div class="card layout-4 h-100 px-0">
                <a href="{{url}}" class="card-link"></a>
                {{#if center}}
                <div class="card-img-wrapper small">
                    {{#if image}}
                    <img src="{{image.url}}" class="card-img-top"
                         alt="{{title}}">
                    {{/if}}
                </div>
                <div class="card-body flex-column h-100">
                    <div class="card-title">
                        <h5 class="text-center">{{title}}</h5>
                    </div>

                    {{#if content}}
                    <div class="card-text text-center">
                        {{{content}}}
                    </div>
                    {{/if}}
                </div>
                <div class="card-footer">
                    {{#if cta_label}}
                    <a href="{{url}}" class="card-footer-link text-center">{{cta_label}}</a>
                    {{/if}}
                </div>

                {{else}}
                <div class="card-img-wrapper">
                    {{#if image}}
                    <img src="{{image.url}}" class="card-img-top"
                         alt="{{title}}">
                    {{/if}}
                </div>
                <div class="card-body flex-column h-100">
                    <div class="card-title">
                        <p>{{title}}</p>
                    </div>

                    {{#if content}}
                    <div class="card-text">
                        {{{content}}}
                    </div>
                    {{/if}}
                </div>

                <div class="card-footer">
                    {{#if cta_label}}
                    <a href="{{url}}" class="card-footer-link">{{cta_label}}</a>
                    {{/if}}
                </div>
                {{/if}}
            </div>
        </div>
            {{/each}}
            {{/ifEquals}}
    </div>
</div>
