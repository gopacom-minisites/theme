<?php

/** @var array $attributes */

?>
<div class="container-md">
<div class="row mx-0 lazyblock-form">
            <div class="col-12">
                <?php if ($attributes['label'] ?: false) { ?>
                    <h2><?= $attributes['label'] ?></h2>
                <?php } ?>
                <?php
                if (file_exists(get_template_directory() . '/forms/' . $attributes['form'] . '-form.php')) {
                    include get_template_directory() . '/forms/' . $attributes['form'] . '-form.php';
                }
                ?>
            </div>
</div>
</div>
