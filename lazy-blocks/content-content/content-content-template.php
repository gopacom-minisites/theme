<div class="container-md">
    <div class="row lazyblock-content-content flex-centered">
        {{#if contentoneblockquote}}
        <div class="col-lg-6 blockquote-line">
            {{else}}
        <div class="col-lg-5">
        {{/if}}
            {{{contentone}}}
        </div>
        {{#if contenttwoblockquote}}
        <div class="col-lg-6 offset-lg-1 blockquote-line">
            {{else}}
        <div class="col-lg-5 offset-lg-1">
        {{/if}}
            {{{contenttwo}}}
        </div>
    </div>
</div>
