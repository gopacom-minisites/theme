<?php

$blockslug = 'content-content';

// Start the block
$block             = [];
$block['title']    = 'Content Content';
$block['icon']     = 'dashicons dashicons-buddicons-friends';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// Holder for the controls
$controls = [];

// Content 1
$control                         = [];
$control['label']                = 'Content One';
$control['name']                 = 'contentone';
$control['type']                 = 'classic_editor';
$control['child_of']             = '';
$control['width']                = '50';

// Make an id.
$control_id = 'control-two-content-content-one';

// Add the control to the controls
$controls[$control_id] = $control;

// Content 2
$control                         = [];
$control['label']                = 'Content Two';
$control['name']                 = 'contenttwo';
$control['type']                 = 'classic_editor';
$control['child_of']             = '';
$control['width']                = '50';

// Make an id.
$control_id = 'control-two-content-content-two';

// Add the control to the controls
$controls[$control_id] = $control;


// Toggle
$control                         = [];
$control['label']                = 'Content One blockquote line?';
$control['name']                 = 'contentoneblockquote';
$control['type']                 = 'toggle';
$control['default']              = false;
$control['checked']              = false;
$control['child_of']             = '';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-two-content-content-one-blue';

// Add the control to the controls
$controls[$control_id] = $control;

// Toggle
$control                         = [];
$control['label']                = 'Content Two blockquote line?';
$control['name']                 = 'contenttwoblockquote';
$control['type']                 = 'toggle';
$control['default']              = true;
$control['checked']              = false;
$control['child_of']             = '';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-two-content-content-two-blue';

// Add the control to the controls
$controls[$control_id] = $control;


// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
