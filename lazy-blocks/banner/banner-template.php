<div class="row lazyblock-banner{{#if control-textmode}} light-mode{{/if}}">
    {{#if backgroundimage}}
    <div class="full-image" style="background: center / cover no-repeat url('{{backgroundimage.url}}');"></div>
    {{else}}
    <div class="full-color {{background-color}}"></div>
    {{/if}}
    <div class="container-md">
        <div class="row mx-0">
        <section class="col-12 col-lg-10 mx-auto text-center">
            <h2>{{title}}</h2>
        </section>
        </div>
    </div>
</div>
