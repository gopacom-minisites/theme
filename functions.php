<?php

// Add the title tag to the wp_head()
use Rapidmail\ApiClient\Client;
use Rapidmail\ApiClient\Exception\ApiException;
use Rapidmail\ApiClient\Exception\IncompatiblePlatformException;

add_theme_support('title-tag');
add_theme_support('post-thumbnails', ['post', 'event', 'job']);

// Include stuff to help us out.
include_once __DIR__ . '/includes/scripts-and-styles.php';
include_once __DIR__ . '/includes/breadcrumbs.php';
include_once __DIR__ . '/includes/helpers.php';
include_once __DIR__ . '/includes/menus.php';
include_once __DIR__ . '/includes/shortcodes.php';
include_once __DIR__ . '/includes/lazyblocks.php';
include_once __DIR__ . '/includes/handlebars-helpers.php';
include_once __DIR__ . '/includes/editor.php';
include_once __DIR__ . '/includes/blog.php';


/////////// CUSTOM STUFF GOES BELOW ///////////

// Please, when you see that some functions or code is grouped,
// put that into an include.


add_filter('get_site_icon_url', 'project_site_icon_url', 100, 3);
function project_site_icon_url($url, $size, $blog_id)
{
    if (!$url) {
        return get_template_directory_uri() . '/assets/img/icons/eu.png';
    }

    return $url;
}
