<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 */

get_header();
?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <section class="container-md">
                <header class="page-header">
                    <h1 class="page-title">
                        <?php _e('Search results for: '); ?>
                        <span class="page-description"><?php echo get_search_query(); ?></span>
                    </h1>
                </header><!-- .page-header -->

                <?php if (have_posts()) : ?>

                    <!-- Add the pagination functions here. -->

                    <!-- Start of the main loop. -->
                    <?php while (have_posts()) : the_post(); ?>

                        <!-- the rest of your theme's main loop -->
                        <div class="col-12 p-2 search-wrapper">
                            <div class="row">
                                <div class="col-lg-3 col-md-4 col-sm-12 col-4">
                                    <div class="search-title">
                                        <h4><a href="<?= the_permalink() ?>"><?= get_the_title() ?></a></h4>
                                    </div>
                                    <div class="search-date"><?= get_the_date(); ?></div>
                                </div>
                                <div class="col-lg-9 col-md-4 sol-sm-12 col-8">

                                    <div class="news-item-summary mt-4">
                                        <?= project_truncate_words(!empty(get_field('summary')) ? get_field('summary') : create_fallback_string(get_the_content()), 100); ?>
                                    </div>
                                    <div class="news-item-visit"><a href="<?= the_permalink() ?>">Read more</a></div>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?>
                    <!-- End of the main loop -->

                    <!-- Add the pagination functions here. -->
                    <?php
                    the_posts_pagination( array(
                                              'screen_reader_text' => __('Pages'),
                                              'mid_size'  => 1,
                                              'prev_text' => __( 'Previous' ),
                                              'next_text' => __( 'Next' ),
                                          ) );

                    ?>
                <?php else : ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php endif; ?>
            </section>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
