<?php
/**
 * @file Here we do stuff to help out with the blog posts and archive.
 */

/**
 * Modify the month link dealio but not in the admin.
 *
 * @param $monthlink
 * @param $year
 * @param $month
 *
 * @return string
 */
function project_month_link_hook($monthlink, $year, $month)
{
  // Don't do anything in the admin.
    if (is_admin()) {
        return $monthlink;
    }

    $url = get_the_permalink();
    return $url . '?pyear=' . $year . '&pmonth=' . $month;
}
add_filter('month_link', 'project_month_link_hook', 10, 3);

/**
 * Do we have a both pyear and pmonth in the url?
 * @return bool
 */
function project_valid_year_and_month_in_query()
{
    return isset($_GET['pyear']) && isset($_GET['pmonth']);
}
