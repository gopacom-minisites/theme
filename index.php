<?php

$seconds_to_cache = 300;
$ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
header("Expires: $ts");
header("Pragma: cache");
header("Cache-Control: max-age=$seconds_to_cache");
get_header();
?>
<div class="container-md">
  <h1><?= pll__('posts') ?></h1>
</div>
<?php
if (have_posts()) {
  // Load posts loop.
    while (have_posts()) {
        the_post();
        ?>
    <div class="container-md">
      <div class="row no-gutters posts-wrapper">
        <div class="col-lg-12">
          <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
          <?php
            $featured_image = get_the_post_thumbnail_url(null, 'full');
            if ($featured_image) {
          ?>
            <div class="img-wrapper">
              <img src="<?= $featured_image ?>" class="figure-img img-fluid" alt="<?= the_title(); ?>">
            </div>
          <?php } ?>
          <div class="mb-5"><p><?php echo strtolower(get_the_date('d M Y')); ?></p></div>
            <?php the_excerpt(); ?>

        </div>
      </div>
    </div>
<?php
    }
} else {
    get_404_template();
}

get_footer();
